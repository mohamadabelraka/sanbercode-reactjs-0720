// Soal 1 //
console.log('LOOPING PERTAMA');
var flag = 2;
while(flag <= 20) {
  console.log(`${flag} - I love coding`);
  flag+=2;
}
console.log('LOOPING KEDUA');
flag-=2;
while (flag >= 2) {
  console.log(`${flag} - I will become a frontend developer`);
  flag-=2;
}

//Spasi untuk nomer selanjutnya
console.log();

// Soal 2 //
for(var i = 1; i <= 20; i++) {
  if (i % 2 == 1) {
    if (i % 3 == 0) {
      console.log(`${i} - I Love Coding`);
    }else {
      console.log(`${i} - Santai`);
    }
  }else if (i % 2 ==0) {
    console.log(`${i} - Berkualitas`);
  }
}

//Spasi untuk nomer selanjutnya
console.log();

// Soal 3 //
for(var baris = 1; baris <= 7; baris++){
  var pagar = '#';
  for(var kolom = 1; kolom < baris; kolom++){
    pagar+='#';
  }
  console.log(pagar);
}

//Spasi untuk nomer selanjutnya
console.log();

// Soal 4 //
var kalimat="saya sangat senang belajar javascript";
console.log(kalimat.split(" "));

//Spasi untuk nomer selanjutnya
console.log();

// Soal 5 //
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var urut = daftarBuah.sort();
print = 0
while(print < 5){
  console.log(daftarBuah[print]);
  print++;
}
