// Soal 1 //
console.log("Soal 1 :");
var arrayDaftarPeserta = {nama :"Asep",
"jenis kelamin" : "laki-laki",
hobi : "baca buku" ,
"tahun lahir" : 1992}
console.log(arrayDaftarPeserta);

// Soal 2 //
console.log("Soal 2 :");
var buah = [{nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000},
{nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000},
{nama: "Semangka", warna: "Hijau & Merah", "ada bijinya": "ada", harga: 10000},
{nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak", harga: 5000}]
console.log(buah[0]);

// Soal 3 //
console.log("Soal 3 :");
var dataFilm = []
function isiDataFilm(nama, durasi, genre, tahun) {
  var isi = {nama : nama, durasi : durasi, genre : genre, tahun : tahun}
  dataFilm.push(isi)
}
isiDataFilm("Avatar", "2 Jam 42 Menit", "Action, Sci-Fi", 2009);
isiDataFilm("Kimi no Nawa", "1 Jam 46 Menit", "Animation, Drama", 2016);
console.log(dataFilm);

// Soal 4 //
console.log("Soal 4 :");
class Animal {
  constructor(name) {
    this._name = name;
    this._legs = 4;
    this._cold_blooded = false;
  }
  get name() {
    return this._name;
  }
  get legs(){
    return this._legs;
  }
  get cold_blooded(){
    return this._cold_blooded;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

class Ape extends Animal {
  constructor(name) {
    super(name);
    this._legs = 2;
  }
  yell(){
    return console.log("Auoo");
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
  }
  jump(){
    return console.log("hop hop");
  }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong); //coba panggil
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
console.log(kodok); //coba panggil
kodok.jump() // "hop hop"


// Soal 5 //
console.log("Soal 5 : ");
class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(()=>this.render(), 1000);
  }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();
