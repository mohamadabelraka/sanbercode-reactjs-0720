var readBooksPromise = require('./promise.js')

var books = [
  {name: 'LOTR', timeSpent: 3000},
  {name: 'Fidas', timeSpent: 2000},
  {name: 'Kalkulus', timeSpent: 4000}
]

//Lanjutkan code untuk menjalankan function readBooksPromise

function readBookAuto(time, index) {
  readBooksPromise(time, books[index])
  .then(function(fulfilled){
    if (index + 1 < books.length) {
      readBookAuto(fulfilled, index+1)
    }
    else {
      console.log("Semua buku sudah dibaca");
    }
  })
  .catch(function(error){
    console.log(`saya butuh ${error*(-1)} untuk bisa membaca`);
  })
}
readBookAuto(10000, 0)
