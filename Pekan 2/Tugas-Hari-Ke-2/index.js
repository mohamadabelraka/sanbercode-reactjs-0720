// di index.js
var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini

function recRead(time, index) {
  readBooks(time, books[index], function (sisaWaktu){
    if (sisaWaktu > 0) {
      if (index + 1 < books.length) {
        recRead(sisaWaktu, index + 1)
      }else {
        console.log("Semua buku sudah dibaca");
      }
    }
    else {
      console.log("saya tidak ada waktu");
    }
  })
}
recRead(10000, 0)
