import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Form Pembelian Buah</h1>
      </header>
      <form action="">
      <table>
        <tr>
          <td>
            <label for="nama"><b>Nama Pelanggan</b></label>
          </td>
          <td>
            <input type="text" name="nama"/>
          </td>
        </tr>
        <tr>
          <td>
          <br/><br/><br/><br/>
            <label for="item"><b>Daftar Item</b></label>
          </td>
          <td>
              <input type="checkbox" name="semangka" value="Semangka"/> Semangka<br/>
              <input type="checkbox" name="jeruk" value="Jeruk"/> Jeruk<br/>
              <input type="checkbox" name="nanas" value="Nanas"/> Nanas<br/>
              <input type="checkbox" name="salak" value="Salak"/> Salak<br/>
              <input type="checkbox" name="anggur" value="Anggur"/> Anggur
          </td>
        </tr>
        <tr>
          <td><input type="submit" value="Kirim" class="submit"/></td>
        </tr>
      </table>
    </form>
    </div>
  );
}

export default App;
