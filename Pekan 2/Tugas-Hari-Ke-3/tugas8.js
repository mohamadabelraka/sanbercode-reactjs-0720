// Soal 1 //
console.log("Soal 1 :");
const lingkaran = (r)=>{
  const phi = 3.14;
  let luas = phi * r * r;
  let keliling = phi * 2 * r;
  return console.log(`Luas dan keliling lingkaran dengan radius ${r} adalah ${luas} dan ${keliling}`);
}
lingkaran(7)

// Soal 2 //
console.log("Soal 2 :");
let kalimat = ""
const tambahKata = ()=>{
  let kata1 = "saya"
  let kata2 = "adalah"
  let kata3 = "seorang"
  let kata4 = "frontend"
  let kata5 = "developer"
  return console.log(`${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`);
}
kalimat = tambahKata()

// Soal 3 //
console.log("Soal 3 :");
class Book {
  constructor(name, totalPage, price) {
    this._name = name;
    this._totalPage = totalPage;
    this._price = price;
  }
}
let pelajaran = new Book('Matematika', 300, 30000)
class Comic extends Book{
  constructor(name, totalPage, price, isColorful) {
    super(name, totalPage, price)
    this.isColorful = true
  }
}
let naruto = new Comic('Naruto', 30, 25000, true)
console.log(pelajaran);
console.log(naruto);
